const express = require("express");

//b2 khởi tạo app express
const app = new express();

//b3: khai báo cỗng để chạy api
const port = 8000;

//cấu hình để sử dụng json
app.use(express.json());

class Drink {
    id;
    maNuocUong;
    tenNuocUong;
    donGia;
    ngayTao;
    ngayCapNhat;
    constructor(id, maNuocUong, tenNuocUong, donGia, ngayTao, ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
}
var drink1 = new Drink(1, "TRATAC", "Trà tắc", 10000, "14/5/2021", "14/5/2021");
var drink2 = new Drink(2, "COCA", "Cocacola", 15000, "14/5/2021", "14/5/2021");
var drink3 = new Drink(3, "PEPSI", "Pepsi", 15000, "14/5/2021", "14/5/2021");


//khởi tạo phương thức get của drinks 
app.get("/drinks-class", (request, response) => {
    let drink1 = request.drink1;
    let drink2 = request.drink2;
    let drink3 = request.drink3;

    response.json({
        drink1, 
        drink2,
        drink3
    })
})